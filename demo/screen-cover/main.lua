--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "X_MAGIC_BOX"
VERSION = "1.0.4"

--根据固件判断模块类型
moduleType = string.find(rtos.get_version(),"8955") and 2 or 4

require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE

require "sys"
require "misc"
require "net"
require"pm"

require"nvm"
require"config"
nvm.init("config.lua")

if nvm.get("test") then --测试通过


require"utils"

require"audio"
audio.setStrategy(1)

--开机加载各个设置
audio.setVolume(nvm.get("vol"))

--此处关闭RNDIS网卡功能
--否则，模块通过USB连接电脑后，会在电脑的网络适配器中枚举一个RNDIS网卡，电脑默认使用此网卡上网，导致模块使用的sim卡流量流失
ril.request("AT+RNDISCALL=0,1")

require"tools"
require"lcd"
require"st7789"
--require"gc9306x"
require"keycfg"

--下面的加载之后，可以把数据刷到1.54寸墨水屏上
--按C键，可以刷新数据
--require"eink"

gps = require"gpsZkw"
--agps功能模块只能配合Air800或者Air530使用；如果不是这两款模块，不要打开agps功能
--require"agpsZkw"

require"ntp"
ntp.timeSync()
--require"bmp280"

require"page"
--require"gpsTest"

--升级检查
PRODUCT_KEY = "xGQtHic8ET6XtqjqByznRRs891EgjUnl"
require "update"
update.request(function (r)
    if r then
        page.change("updateNotice")
    end
end)

else--没测试过的话
    require "color_lcd_spi_st7735"
    require "test"
end

--启动系统框架
sys.init(0, 0)
sys.run()
