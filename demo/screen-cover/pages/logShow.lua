module(...,package.seeall)
require"lcd"
require"common"

local MAX_COL,MAX_ROW = 240/lcd.CHAR_WIDTH,240/lcd.CHAR_WIDTH/2
local old_log = log.info
local old_warn = log.warn
local logTemp = {}
local timer_id

-- --tf卡文件
local tfFile

--是否需要刷新
local refresh

local function cropTemp() while #logTemp > MAX_ROW do table.remove(logTemp,1) end end
local function addNewLine(s)
    if tfFile and nvm.get("saveLog") then
        local f = io.open(tfFile,"a+b")
        if f then
            f:write("[")
            f:write(tostring(os.time()))
            f:write("]")
            f:write(s)
            f:write("\n")
            f:close()
        end
    end
    s = common.utf8ToGb2312(s)
    s = s:gsub("\r","")
    s = s:split("\n")
    for i=1,#s do
        if s[i]:len() > 0 or i ~= #s then--防止行末尾是回车换行
            while true do--截断字符串直到小于一行
                cropTemp()
                if s[i]:len() > MAX_COL then
                    table.insert(logTemp,s[i]:sub(1,MAX_COL))
                    s[i] = s[i]:sub(MAX_COL+1)
                else
                    table.insert(logTemp,s[i])
                    break
                end
            end
        end
    end
    cropTemp()
    refresh = true
end

function update()
    disp.setcolor(lcd.rgb(0,255,0))
    disp.clear()
    for i=1,#logTemp do
        disp.puttext(logTemp[i],0,(i-1)*lcd.CHAR_WIDTH*2)
    end
    lcd.putStringRight(tostring(misc.getVbatt()),240,0,0,0,255)
end

function open()
    disp.setbkcolor(0)
    disp.setcolor(0xffff)
    timer_id = sys.timerLoopStart(function()
        if not refresh then return end
        refresh = nil
        page.update()
    end, 50)
end

function close()
    if timer_id then
        sys.timerStop(timer_id)
        timer_id = nil
    end
    refresh = nil
end

--重定向日志打印函数
log.info = function(tag, ...)
    old_log(tag, ...)
    local arg = {...}
    local to_print = {}
    for i=1,#arg do
        if type(arg[i]) == "string" and arg[i]:len() >= MAX_COL*MAX_ROW then
            arg[i] = arg[i]:sub(-MAX_COL*MAX_ROW,-1)
        end
        table.insert(to_print,tostring(arg[i]))
    end
    addNewLine("["..tostring(tag).."]"..table.concat(to_print," "))
end

log.warn = function(tag, ...)
    old_warn(tag, ...)
    local arg = {...}
    local to_print = {}
    for i=1,#arg do
        if type(arg[i]) == "string" and arg[i]:len() >= MAX_COL*MAX_ROW then
            arg[i] = arg[i]:sub(-MAX_COL*MAX_ROW,-1)
        end
        table.insert(to_print,tostring(arg[i]))
    end
    addNewLine("["..tostring(tag).."]"..table.concat(to_print," "))
end

sys.taskInit(function()
    sys.waitUntil("LCD_INIT")
    while os.time() < 1616033252 do
        sys.wait(100)
    end
    local t = os.date("*t")
    t = string.format("%04d%02d%02d_%02d%02d%02d",
        t.year,t.month,t.day,
        t.hour,t.min,t.sec)
    tfFile = "/sdcard0".."/"..t..".log"
end)

