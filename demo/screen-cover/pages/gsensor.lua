module(...,package.seeall)
require"tools"

local timer_id

local enable = true
local i2cslaveaddr = 0x18
local function i2cRead(addr,len)
    i2c.send(tools.i2cid,i2cslaveaddr,addr)
    return i2c.recv(tools.i2cid,i2cslaveaddr,len)
end
local function i2cWrite(addr,...)
    return i2c.send(tools.i2cid,i2cslaveaddr,{addr,...})
end
i2cWrite(0x1E,0x05)--打开操作权限，0x1E 寄存器写 0x05
local sl_val = i2cRead(0x57,1) --读取 0x57 寄存器当前配置
if #sl_val > 0 then
    log.info("sl_val", sl_val:toHex())
    sl_val = bit.bor(string.byte(sl_val),0x40) --I2C_PU 置 1
    i2cWrite(0x57,sl_val)

    --设置参数
    i2cWrite(0x20,0x37)
    i2cWrite(0x21,0x11)
    i2cWrite(0x22,0x40)
    i2cWrite(0x23,0x88)
else
    enable = false
end

--屏幕宽高
local rw,rh = disp.getlcdinfo()
--球xy加速度、球位置
local bx,by,bxa,bya = rw/2,rh/2,0,0
--加速度系数（实际为百分之一）
local aa = 3
--球图片大小
local ballSize = 25

function update()
    if not enable then
        disp.clear()
        lcd.putStringCenter("板载加速度计无效",120,120,255,255,255)
        return
    end
    local data = i2cRead(0xa8,6)
    --log.info("read i2c",data:toHex())
    if #data ~= 6 then return end
    --Convert the data to 10 bits
    data = string.char(
        data:byte(1)%8,data:byte(2),
        data:byte(3)%8,data:byte(4),
        data:byte(5)%8,data:byte(6)
    )
    local _,xa,ya,za = pack.unpack(data,">HHH")
    if xa > 127 then xa = xa - 256 end
    if ya > 127 then ya = ya - 256 end
    if za > 127 then za = za - 256 end
    --log.info("xyz",xa,ya,za)

    bxa,bya = bxa+ya*aa/100,bya+xa*aa/100--当前加速度+测量加速度
    --计算位移
    bx,by = bx+bxa,by+bya
    --边缘反弹计算
    if bx < 0 then bx = -bx/1.2 bxa = -bxa/1.2 end
    if by < 0 then by = -by/1.2 bya = -bya/1.2 end
    if bx > rw-ballSize then bx = rw-ballSize-(bx-(rw-ballSize))/1.2 bxa = -bxa/1.2 end
    if by > rh-ballSize then by = rh-ballSize-(by-(rh-ballSize))/1.2 bya = -bya/1.2 end
    disp.clear()
    lcd.putStringCenter("重力:"..aa,120,120,255,255,255)
    disp.putimage("/lua/ball"..tostring(ballSize)..".png",bx,by)
end

function key(k,e)
    if not e then return end
    if k == "4" or k == "LEFT" then
        aa = aa - 1
    elseif k == "5" or k == "RIGHT" then
        aa = aa + 1
    end
end

function open()
    timer_id = sys.timerLoopStart(page.update,5)
end

function close()
    if timer_id then
        sys.timerStop(timer_id)
        timer_id = nil
    end
end

