module(...,package.seeall)

--当前的灯状态

function update()
    disp.clear()
    disp.setfontheight(32)
    lcd.CHAR_WIDTH = 16
    lcd.putStringCenter("升级提示",120,lcd.gety(0),255,255,255)

    lcd.putStringCenter("升级文件下载完成",120,lcd.gety(3),255,0,0)
    lcd.putStringCenter("按确定键重启",120,lcd.gety(5),255,0,0)

    disp.setfontheight(16)
    lcd.CHAR_WIDTH = 8
end


local keyEvents = {
    ["A"] = function ()
        sys.restart("update")
    end,
}
keyEvents["3"] = keyEvents["A"]
keyEvents["OK"] = keyEvents["A"]

function key(k,e)
    if not e then return end
    if keyEvents[k] then
        keyEvents[k]()
        page.update()
    end
end

