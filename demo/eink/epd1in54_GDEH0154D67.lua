module(...,package.seeall)

require"misc"
require"utils"
require"pins"

--电压域
pmd.ldoset(15,pmd.LDO_VLCD)

log.info("spi.setup",spi.setup(spi.SPI_1,0,0,8,13000000,0,0))

local function getBusyFnc(msg)
    log.info("Gpio.getBusyFnc",msg)
    if msg==cpu.INT_GPIO_POSEDGE then--上升沿中断
        --不动作
    else--下降沿中断
        sys.publish("BUSY_DOWN")
    end
end

--初始化三个控制引脚
local getBusy         = pins.setup(7,getBusyFnc)
local setRST          = pins.setup(12,1)
local setDC           = pins.setup(18,1)

-- Display resolution
EPD_WIDTH       = 200
EPD_HEIGHT      = 200

local function wait()
    while getBusy() == 1 do  -- 0: idle, 1: busy
        sys.waitUntil("BUSY_DOWN",5000)
    end
end

setDC(1)
local function sendCommand(data)
    --log.info("epd1in45.sendCommand",data)
    setDC(0)
    spi.send(spi.SPI_1,string.char(data))
end

local function sendData(data)
    --log.info("epd1in45.sendData",data)
    setDC(1)
    spi.send(spi.SPI_1,string.char(data))
end

local function sendDataString(data)
    --log.info("epd1in45.sendData",data)
    setDC(1)
    spi.send(spi.SPI_1,data)
end

local function sendDataTrans(data)
    setDC(1)
    local temp
    for i=1,200 do
        for j=25,1,-1 do
            temp = (j-1)*200+i
            --log.info("epd1in45.temp",temp)
            spi.send(spi.SPI_1,data:sub(temp,temp))
        end
    end
end

local function reset()
    log.info("epd1in45.reset","")
    setRST(1)
    sys.wait(20)
    setRST(0)
    sys.wait(5)
    setRST(1)
    sys.wait(20)
end

function deepSleep()
    log.info("epd1in45.deepSleep","")
    sendCommand(0x10) --enter deep sleep
    sendData(0x01)
    sys.wait(100)
    setDC(0)
end

local lut = {
0x80,0x48,0x40,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x40,0x48,0x80,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x80,0x48,0x40,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x40,0x48,0x80,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0xA,0x0,0x0,0x0,0x0,0x0,0x0,
0x8,0x1,0x0,0x8,0x1,0x0,0x2,
0xA,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x0,0x0,0x0,0x0,0x0,0x0,0x0,
0x22,0x22,0x22,0x22,0x22,0x22,0x0,0x0,0x0,
0x22,0x17,0x41,0x0,0x32,0x20
}
local function setLut(lut)
    sendCommand(0x32)
    for i=1,153 do
        sendData(lut[i])
    end
    wait()
    sendCommand(0x3f)
    sendData(lut[154])
    sendCommand(0x02)
    sendData(lut[155])
    sendCommand(0x04)
    sendData(lut[156])
    sendData(lut[157])
    sendData(lut[158])
    sendCommand(0x2c)
    sendData(lut[159])
end

function init()
    log.info("epd1in45.init","")
    reset()

    wait()
    sendCommand(0x12) --SWRESET
    wait()

    sendCommand(0x01) --Driver output control
    sendData(0xC7)
    sendData(0x00)
    sendData(0x00)

    sendCommand(0x11) --data entry mode
    sendData(0x03)

    sendCommand(0x44) --set Ram-X address start/end position
    sendData(0x00)
    sendData(0x18)    --0x0C-->(18+1)*8=200

    sendCommand(0x45) --set Ram-Y address start/end position
    sendData(0x00)
    sendData(0x00)
    sendData(0xC7)--0xC7-->(199+1)=200
    sendData(0x00)

    sendCommand(0x3C) --BorderWavefrom
    sendData(0x01)

    sendCommand(0x18) --Reading temperature sensor
    sendData(0x80)

    sendCommand(0x4E)   -- set RAM x address count to 0;
    sendData(0x00)
    sendCommand(0x4F)   -- set RAM y address count to 0X199;
    sendData(0xC7)
    sendData(0x00)

    wait()

    setLut(lut)
    log.info("epd1in45.init","done")
end

function display_frame()
    log.info("epd1in45.display_frame","start")
    sendCommand(0x22)
    sendData(0xF7)
    sendCommand(0x20)
    wait()
    log.info("epd1in45.display_frame","done")
end

function clear(color)
    sendCommand(0x24)
    for _=1,EPD_WIDTH*EPD_HEIGHT/8 do
        sendData(color)
    end
    sendCommand(0x26)
    for _=1,EPD_WIDTH*EPD_HEIGHT/8 do
        sendData(color)
    end
    display_frame()
    log.info("epd1in45.clear","done")
end

--输入值：数组
function showPictureN(pic)
    wait()
    log.info("epd1in45.showPicture","")
    sendCommand(0x24)
    for i=1,#pic do
        sendData(pic[i])
    end
    display_frame()
end

--输入值：string
function showPicture(pic)
    wait()
    log.info("epd1in45.showPicture","")
    sendCommand(0x24)
    sendDataString(pic)
    display_frame()
end

--输入值：string,按页显示的数据
function showPicturePage(pic)
    wait()
    log.info("epd1in45.showPicturePage","")
    sendCommand(0x24)
    sendDataTrans(pic)
    display_frame()
end


